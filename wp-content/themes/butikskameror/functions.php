<?php 

if ( !is_admin() ) wp_deregister_script('jquery');

register_nav_menu( 'main-navigation', 'Huvudnavigationen' );
register_nav_menu( 'sitemap', 'Footer Sitemap' );

function mv_my_theme_styles()
{
	if (!is_admin())
   	 wp_enqueue_style(
	'my-custom-style', get_template_directory_uri() . '/css/main.css',false,'0.5','all');
}
add_action('wp_enqueue_scripts','mv_my_theme_styles');