		</div><!-- .content -->
	</div><!-- .container -->
	<div class="push"></div>
</div>
	<div class="footer">
		<div class="footer-container">
			<div class="col3 first">
				<h2>Om företaget</h2>
				<p>Butikskameror.se startades 2013 som ett resultat av den ökande efterfrågan på kamera-övervakning främst i butiker efter att en ny kameraövervakningslag trätt i kraft
					<a href="/om-foretaget">Läs mer...</a></p>
			</div>
			<div class="col3">
				<h2>Kontakt</h2>
				<dl>
					<dt>Mail</dt>
					<dd>info@butikskameror.se</dd>
					<dt>Tfn.</dt>
					<dd>0705 – 40 40 04</dd>
				</dl>
				<p><a href="<?php echo get_permalink( get_page_by_title('Kontakt') ) ?>">Gå till kontaktformulär</a></p>
			</div>
			<div class="col3">
				<?php wp_nav_menu( array( 'menu' => 'sitemap', 'menu_class' => 'footer-sitemap clearfix' ) ); ?>
				<p class="footer-copy">Skapad av <a href="http://www.appearance.se">Appearance</a> och <a href="http://christoffer.rydberg.me">Christoffer Rydberg</a></p>
			</div>
		</div>
	</div>

<?php wp_footer(); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>
</body>
</html>