<?php get_header(); ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="startpage">
			<div class="col2_startpage_left first startpage-content">
				<?php the_content(); ?>
			</div>
			<div class="col2_startpage_right">
				<form id="price-calculator" class="startpage-form">
					<div class="startpage-form-set first-form-set">
						<label for="camera-amount">Antal kameror</label>
						<!--[if !IE]> --> <div class="notIE"> <!-- <![endif]-->
						<label />
						<select name="camera-amount" id="camera-amount">
							<option value="5500">1</option>
							<option value="9000">2</option>
							<option value="12500">3</option>
							<option value="16000">4</option>
							<option value="19500">5</option>
							<option value="23000">6</option>
							<option value="26500">7</option>
							<option value="30000">8</option>
							<option value="33500">9</option>
							<option value="37000">10</option>
							<option value="40500">11</option>
							<option value="44000">12</option>
							<option value="47500">13</option>
							<option value="51000">14</option>
							<option value="54500">15</option>
							<option value="58000">16</option>
							<option value="0">Fler än 16</option>
						</select>
						<!--[if !IE]> --></div> <!-- <![endif]-->

					</div>

					<div class="startpage-form-set">
						<label for="area">Butiksyta (kvm)</label>
						<!--[if !IE]> --> <div class="notIE"> <!-- <![endif]-->
						<label />
						<select name="area" id="area">
							<option value="0">0-50</option>
							<option value="1000">51-100</option>
							<option value="1500">101-150</option>
							<option value="3000">151-250</option>
							<option value="4000">251-500</option>
							<option value="5000">501-750</option>
							<option value="6000">751-1000</option>
							<option value="1">Mer än 1000</option>
						</select>
						<!--[if !IE]> --></div> <!-- <![endif]-->

					</div>

					<div class="startpage-form-set">
						<label>Tillgång till dator?</label>
						<div class="inputs">
							<label class="label_with_for" for="has-computer-yes">Ja</label>
							<input type="radio" name="has-computer" value="1" id="has-computer-yes" class="has-computer-yes">
							<label class="label_with_for" for="has-computer-no">Nej</label>
							<input type="radio" name="has-computer" value="1" id="has-computer-no" class="has-computer-no">
						</div>
					</div>

					<div class="startpage-form-set">
						<label>Placering i Örebro Län?</label>
						<div class="inputs">
							<label class="label_with_for" for="in-orebro-yes">Ja</label>
							<input type="radio" name="in-orebro" value="1" id="in-orebro-yes" class="in-orebro-yes">
							<label class="label_with_for" for="in-orebro-no">Nej</label>
							<input type="radio" name="in-orebro" class="in-orebro-no" value="1" id="in-orebro-no">
						</div>
					</div>

					<div class="startpage-form-set distance-from-orebro-input">
						<label class="label_with_for" for="distance-from-orebro">km från Örebro?</label>
						<input type="text" name="distance-from-orebro" id="distance-from-orebro">
					</div>

					<input type="submit" id="price-calculator-submit" class="btn" value="Beräkna pris">
				</form>
			</div>
		</div>

		<div class="row startpage-info-boxes">
			<div class="col3 first startpage-info-box">
				<i><img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" width="50" alt="Hör av dig till oss"></i>
				<h2>Hör av dig till oss</h2>
				<p>
					Tveka inte att höra av dig till oss om du har frågor, vill boka ett personligt möte eller beställa en kostnadsfri offertuträkning.
					<a href="<?php echo get_permalink( get_page_by_title('Kontakt') ) ?>">Läs mer...</a>
				</p>
			</div>
			<div class="col3 startpage-info-box">
				<i><img src="<?php echo get_template_directory_uri(); ?>/img/check.png" width="50" alt="Vad ingår i priset?"></i>
					<h2>Vad ingår i priset?</h2>
					<p>
						Vi gör butiksövervakning enkelt för våra kunder. Därför ingår alltid installation, utbildning och support i våra priser.
						<a href="<?php echo get_permalink( get_page_by_title('Vad ingår i priset?') ) ?>">Läs mer...</a>
					</p>
			</div>
			<div class="col3 startpage-info-box">
				<i><img src="<?php echo get_template_directory_uri(); ?>/img/kamera.png" width="50" alt="Den nya kamera lagen"></i>
					<h2>Den nya kameralagen</h2>
					<p>
						Den största förändring som skett är att kameraövervakning i butiker som förut krävde tillstånd nu har mildrats avsevärt.
						<a href="<?php echo get_permalink( get_page_by_title( 'Nya kameralagen' ) ); ?>">Läs mer...</a>
					</p>
			</div>
		</div>

		<div class="result-popup">
			<div class="popup-close">Stäng</div>
			<div class="result-popup-container">
				<p class="info">Priset är endast preliminärt och vägledande. Offerten är ej bindande.</p>
				<div class="result"><span class="result-price">15000</span> SEK</div>
				<div class="result-tax">exkl. moms</div>
				<?php echo do_shortcode(['[contact-form-7 id="30" title="Popup Get-In-Touch"]'])[0]; ?>
				<div class="popup-links">
					<a href="<?php echo get_permalink( get_page_by_title('Kontakt') ) ?>">Kontakt</a>
					<a href="<?php echo get_permalink( get_page_by_title('Vad ingår i priset?') ) ?>">Vad ingår i priset?</a>
				</div>
			</div>
		</div>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
