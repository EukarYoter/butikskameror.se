<!DOCTYPE html>
<html lang="sv">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title>Really nice Title of Doom</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- For IE -->
<!--[if IE]><link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico"><![endif]-->
<!-- For Modern Browsers with PNG Support -->
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

<?php wp_head(); ?>
</head>

<body>
<div class="wrapper">
	<div class="container">
		<div class="header">
			<div class="logo">
				<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" width="200" height="80" alt="Butikskameror i Örebro"></a>
			</div>
			<div class="navigation">
				<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'bjorn_sthlm' ); ?>"><?php _e( 'Skip to content', 'bjorn_sthlm' ); ?></a></div>
				<?php wp_nav_menu( array( 'menu' => 'main-nav', 'menu_class' => 'main-nav' ) ); ?>
			</div>
		</div>
		<div class="content">
