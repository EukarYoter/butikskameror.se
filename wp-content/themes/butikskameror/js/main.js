$(function() {

	$('#price-calculator').on('submit', function() {
		var formData = $(this).serialize(),
			camera_amount = $('#camera-amount'),
			area = $('#area'),
			has_computer = $('#has-computer-yes'),
			in_orebro_county_no = $('#in-orebro-no'),
			distance_from_orebro = $('#distance-from-orebro');

		var total_sum = parseInt(camera_amount.val()) + parseInt(area.val());
			if( !has_computer.is(':checked'))
				total_sum += 4000;

			if(in_orebro_county_no.is(':checked'))	{
				if(parseInt(distance_from_orebro.val()) > 150)
					total_sum += parseInt(distance_from_orebro.val()) * 8 + 500;
				else {
					total_sum += parseInt(distance_from_orebro.val()) * 8;
				}
			}

			$('.result-popup').fadeIn(200);
			if ( $('#camera-amount').val() != 0 && $('#area').val() != 1 ) {
				$('.result').removeClass('result-text');
				$('.result').html('<span class="result-price">'+total_sum+'</span> SEK');
				$('.result-tax').show();
			} else if($('#camera-amount').val() == 0) {
				$('.result').addClass('result-text');
				$('.result-tax').hide();
				$('.result').html('Mängden kameror gör det svårt för oss att uppskatta ett pris. Vänligen kontakta oss så ger vi dig ett kostnadsfritt offertförslag.');
			} else if($('#area').val() == 1) {
				$('.result').addClass('result-text');
				$('.result-tax').hide();
				$('.result').html('Den stora lokalytan gör det svårt för oss att uppskatta ett pris. Vänligen kontakta oss så ger vi dig ett kostnadsfritt offertförslag.');
			}

			// Värden för mailet som skickas ut.
			$('#offer-price').val(total_sum);
			$('#mail-camera-amount').val(camera_amount.find(":selected").text());
			$('#mail-area').val(area.find(":selected").text());
			var mail_has_computer = has_computer.is(':checked') ? "Ja" : "Nej";
			$('#mail-has-computer').val(mail_has_computer);
			var in_orebro = in_orebro_county_no.is(':checked') ? "Nej" : "Ja";
			$('#mail-in-orebro').val(in_orebro);
			$('#mail-distance-from-orebro').val(distance_from_orebro.val());
			
		return false;
	});

	$('#in-orebro-no').on('change', function() {
		if( $(this).is(':checked') )
			$('.distance-from-orebro-input').fadeIn(200);
		else
			$('.distance-from-orebro-input').fadeOut(200);
	});

	$('#in-orebro-yes').on('change', function() {
		if( $(this).is(':checked') )
			$('.distance-from-orebro-input').fadeOut(200);
		else
			$('.distance-from-orebro-input').fadeIn(200);
	});

	$('body').on('click', '.popup-close', function() {
		$('.result-popup').fadeOut(200);
	});
});