<?php
/**
 * Baskonfiguration f�r WordPress.
 *
 * Denna fil inneh�ller f�ljande konfigurationer: Inst�llningar f�r MySQL,
 * Tabellprefix, S�kerhetsnycklar, WordPress-spr�k, och ABSPATH.
 * Mer information p� {@link http://codex.wordpress.org/Editing_wp-config.php 
 * Editing wp-config.php}. MySQL-uppgifter f�r du fr�n ditt webbhotell.
 *
 * Denna fil anv�nds av wp-config.php-genereringsskript under installationen.
 * Du beh�ver inte anv�nda webbplatsen, du kan kopiera denna fil direkt till
 * "wp-config.php" och fylla i v�rdena.
 *
 * @package WordPress
 */

// ** MySQL-inst�llningar - MySQL-uppgifter f�r du fr�n ditt webbhotell ** //
/** Namnet p� databasen du vill anv�nda f�r WordPress */
define('DB_NAME', 'dev_butikskamero');

/** MySQL-databasens anv�ndarnamn */
define('DB_USER', 'dev_butikskamero');

/** MySQL-databasens l�senord */
define('DB_PASSWORD', 'aqXTP99hBNTVJwyG');

/** MySQL-server */
define('DB_HOST', 'localhost');

/** Teckenkodning f�r tabellerna i databasen. */
define('DB_CHARSET', 'utf8');

/** Kollationeringstyp f�r databasen. �ndra inte om du �r os�ker. */
define('DB_COLLATE', '');

/**#@+
 * Unika autentiseringsnycklar och salter.
 *
 * �ndra dessa till unika fraser!
 * Du kan generera nycklar med {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Du kan n�r som helst �ndra dessa nycklar f�r att g�ra aktiva cookies obrukbara, vilket tvingar alla anv�ndare att logga in p� nytt.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l?~1WEY2t;6W&JS}_x wQCp]0hoW-;n`O``n- -*sGybJ4<|J|idD]m+[+k~lFh@');
define('SECURE_AUTH_KEY',  ' &`cvO#*_;D*Hf3>$t+-o=Fj-pURjOp.}fRw;!(wI=KS2{g]iylpQX#s?[CkNh#j');
define('LOGGED_IN_KEY',    '3cx2b,]||o-7KS+o(_Uc1oMJ9y*)^E%8dH=co>#~,yl87|IHjy^>;QA3L>Y3@!:!');
define('NONCE_KEY',        '{B+XE(F/qHIegeCE7@nmDZ}<A@jYG3a@OH=gvAE)aOui@cG&w_Q+|y|%^!4{.07a');
define('AUTH_SALT',        'thuwRm{m-lW`1kJ9#0(:)nIQ:-`![Pf2i>Zw$hg|JYB>?|b]SR]xT>aV9H!x2[+m');
define('SECURE_AUTH_SALT', 'ScvSnH!,3;V:TlZ1,.l^,@J#e05{!eukz.PfI,!-JYt.s5vW&9NTi9WfZftd8,>P');
define('LOGGED_IN_SALT',   'gdhpg/C[50*i=;_) lInH0-3.Xc,7O{]xPuC+8cl~Uz*_g9aO)]94omt7s-><^{p');
define('NONCE_SALT',       'yKxE|Sjx.=*t|cX%;y&Zz=GRGv]r&qFQ]<$TZ!CmN]/^ZDFz-O;r1oT$+9a|sjk_');

/**#@-*/

/**
 * Tabellprefix f�r WordPress Databasen.
 *
 * Du kan ha flera installationer i samma databas om du ger varje installation ett unikt
 * prefix. Endast siffror, bokst�ver och understreck!
 */
$table_prefix  = 'wp_';

/**
 * WordPress-spr�k, f�rinst�llt f�r svenska.
 *
 * Du kan �ndra detta f�r att �ndra spr�k f�r WordPress.  En motsvarande .mo-fil
 * f�r det valda spr�ket m�ste finnas i wp-content/languages. Exempel, l�gg till
 * sv_SE.mo i wp-content/languages och ange WPLANG till 'sv_SE' f�r att f� sidan
 * p� svenska.
 */
define('WPLANG', 'sv_SE');

/** 
 * F�r utvecklare: WordPress fels�kningsl�ge. 
 * 
 * �ndra detta till true f�r att aktivera meddelanden under utveckling. 
 * Det �r rekommderat att man som till�ggsskapare och temaskapare anv�nder WP_DEBUG 
 * i sin utvecklingsmilj�. 
 */ 
define('WP_DEBUG', false);

/* Det var allt, sluta redigera h�r! Blogga p�. */

/** Absoluta s�kv�g till WordPress-katalogen. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Anger WordPress-v�rden och inkluderade filer. */
require_once(ABSPATH . 'wp-settings.php');
